<?php

/**
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/public
 */

/**
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/public
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
class Wp_Survey_Shortcode {
	public static function render_test( $atts ) {
		if (!is_page()) {
			return;
		}

		$test = Wp_Survey_DB::get_test( $atts['id'] );
		$limit = isset( $atts['limit'] ) ? $atts['limit'] : get_option( 'wpt_questions_per_test');
		$user_id = wp_get_current_user()->ID;
		$processor = new Wp_Survey_Processor($test['id'], $user_id , $limit);
		$questions = $processor->get_questions();

		if (isset( $_POST['wpt'])) {
			$result = $processor->finish( $_POST['wpt']['question']);
			$result['data'] = json_decode( $result['data'], true);
			Wp_Survey_Helper::public_render( 'result.php', [
				'test' => $test,
				'result' => $result
			] );
		} else {
			Wp_Survey_Helper::public_render( 'test.php', [
				'test' => $test,
				'result_id' => $test,
				'questions' => $questions
			] );
		}
	}
}
