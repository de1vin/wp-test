<?php

/**
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/public
 */

/**
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/public
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
class Wp_Survey_Processor {
	private $result;
	private $questions = [];

	/**
	 * Wp_Survey_Processor constructor.
	 *
	 * @param int $test_id
	 * @param int $user_id
	 * @param int $limit
	 */
	public function __construct($test_id, $user_id, $limit) {
		$result = Wp_Survey_DB::get_unfinished_test( $test_id, $user_id);

		if (empty( $result) ) {
			$result = Wp_Survey_DB::start_new_test( $test_id, $user_id, $limit);
		}

		$this->result = $result;
		$this->result['data'] = json_decode( $this->result['data'], true );
	}

	public function get_questions() {
		if (empty( $this->questions )) {
			$this->load_questions();
		}

		return $this->questions;
	}


	public function finish($answers) {
		$value = $this->validate( $answers);
		return Wp_Survey_DB::finish_result( $this->result['id'], $answers, $value);
	}

	private function load_questions() {
		foreach (array_keys( $this->result['data']['questions']) as $question_id) {
			$this->questions[$question_id] = Wp_Survey_DB::get_question( $question_id);
			$this->questions[$question_id]['answers'] = Wp_Survey_DB::random_answers( $question_id );
		}
	}

	private function validate($answers) {
		$question_ids = array_keys( $this->result['data']['questions']);
		$valid = 0;

		foreach ($question_ids as $question_id) {
			$valid_answers = array_column(Wp_Survey_DB::get_valid_answers($question_id), 'id');
			$user_answers = isset( $answers[$question_id]) ? $answers[$question_id] : [];
			sort($valid_answers);
			sort($user_answers);

			if ($valid_answers==$user_answers) {
				$valid++;
			}
		}

		return $valid;
	}
}
