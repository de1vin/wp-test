(function( $ ) {
	'use strict';

	$(document).on("submit", ".wpt__form", function (e) {
		e.preventDefault();
		const form = $(this);
		$.ajax({
			url: form.attr("action"),
			method: "post",
			data: form.serialize()
		}).done(function(data) {
			form.parents('.wpt').html($(data).find('.wpt').html());
		});
	})

})( jQuery );
