<div class="wpt">
    <form action="" class="wpt__form" method="post">
        <input type="hidden" name="wpt[test]" value="<?php echo $test['id']?>">
        <div class="wpt__title"><?php echo $test['title']; ?></div>
        <?php foreach ($questions as $qn => $question ) : ?>
        <div class="wpt__questions">
            <div class="wpt__question">
                <div class="wpt__question-content"><?php echo $question['content']; ?></div>
                <div class="wpt__answers">
                <?php foreach ($question['answers'] as $an => $answer ) : ?>
                    <div class="wpt__answer<?php if( current_user_can('administrator') && $answer['is_valid']) echo ' wpt__answer--valid' ?>">
                        <?php $checkbox_id = 'wpt_answer_' . $test['id'] . '_' . $question['id'] . '_' . $answer['id']; ?>
                        <input type="checkbox"
                               name="wpt[question][<?php echo $question['id']?>][]"
                               value="<?php echo $answer['id']?>"
                               id="<?php echo $checkbox_id?>">
                        <label for="<?php echo $checkbox_id ?>"><?php echo $answer['content']; ?></label>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <button class="wpt__submit" type="submit">Submit</button>
    </form>
</div>
