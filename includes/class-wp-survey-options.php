<?php

/**
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 */

/**
 * @since      1.0.0
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
class Wp_Survey_Options {
	public static function render_settings_fields() {
		$limit = get_option( 'wpt_questions_per_test');
		echo '<input type="number" name="wpt_questions_per_test" id="wpt_questions_per_test" value="' . $limit . '">';
	}
}
