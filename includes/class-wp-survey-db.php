<?php

/**
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 */

/**
 * @since 1.0.0
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
class Wp_Survey_DB {
	/**
	 * @since 1.0.0
	 * @return string
	 */
	public static function question_table_name() {
		global $wpdb;
		return $wpdb->prefix . 'survey_question';
	}

	/**
	 * @return string
	 */
	public static function question_answer_table_name() {
		global $wpdb;
		return $wpdb->prefix . 'survey_question_answer';
	}

	/**
	 * @return string
	 */
	public static function survey_table_name() {
		global $wpdb;
		return $wpdb->prefix . 'survey';
	}

	/**
	 * @return string
	 */
	public static function result_table_name() {
		global $wpdb;
		return $wpdb->prefix . 'survey_result';
	}

	/**
	 * @param int $id
	 *
	 * @return array|null
	 */
	public static function get_test($id) {
		global $wpdb;
		return $wpdb->get_row('SELECT * FROM ' . self::survey_table_name() . ' t WHERE t.id = ' . (int) $id, ARRAY_A );
	}

	/**
	 * @param int $id
	 *
	 * @return array|null
	 */
	public static function get_question($id) {
		global $wpdb;
		return $wpdb->get_row('SELECT * FROM ' . self::question_table_name() . ' q WHERE q.id = ' . (int) $id, ARRAY_A );
	}

	/**
	 * @param int $id
	 *
	 * @return array|null
	 */
	public static function get_result($id) {
		global $wpdb;
		return $wpdb->get_row('SELECT * FROM ' . self::result_table_name() . ' r WHERE r.id = ' . (int) $id, ARRAY_A );
	}

	/**
	 * @since 1.0.0
	 * @param string $title
	 *
	 * @return bool|int
	 */
	public static function add_test( $title ) {
		global $wpdb;
		$result = $wpdb->insert( self::survey_table_name(), [ 'title' => $title ] );

		if ( $result === false ) {
			return false;
		}

		return $wpdb->insert_id;
	}

	/**
	 * @since 1.0.0
	 * @param int $id
	 * @param string $title
	 *
	 * @return bool
	 */
	public static function update_test( $id, $title ) {
		global $wpdb;
		return (bool)$wpdb->update( self::survey_table_name(), [ 'title' => $title ], [ 'id' => (int)$id ] );
	}

	/**
	 * @since 1.0.0
	 * @param int $id
	 * @param string $content
	 *
	 * @return bool
	 */
	public static function update_question( $id, $content ) {
		global $wpdb;
		return (bool)$wpdb->update( self::question_table_name(), [ 'content' => $content ], [ 'id' => (int)$id ] );
	}

	/**
	 * @since 1.0.0
	 * @param int $id
	 * @param string $content
	 * @param bool $is_valid
	 *
	 * @return bool
	 */
	public static function update_answer( $id, $content, $is_valid ) {
		global $wpdb;
		return (bool)$wpdb->update( self::question_answer_table_name(), [ 'content' => $content, 'is_valid' => $is_valid ], [ 'id' => (int)$id ] );
	}

	/**
	 * @since 1.0.0
	 * @param int $test_id
	 * @param string $content
	 *
	 * @return bool|int
	 */
	public static function add_question( $test_id, $content ) {
		global $wpdb;
		$result = $wpdb->insert( self::question_table_name(), [ 'survey_id' => (int)$test_id, 'content' => $content ] );

		if ( $result === false ) {
			return false;
		}

		return $wpdb->insert_id;
	}

	/**
	 * @param int $question_id
	 * @param string $content
	 * @param bool $is_valid
	 */
	public static function add_answer( $question_id, $content, $is_valid) {
		global $wpdb;
		$content = (array)$content;

		foreach ( $content as $value ) {
			$wpdb->insert( self::question_answer_table_name(), [ 'question_id' => $question_id,  'content' => $value, 'is_valid' => (bool)$is_valid ] );
		}
	}

	/**
	 * @param int $id
	 */
	public static function delete_test($id) {
		global $wpdb;
		$wpdb->delete( self::survey_table_name(), ['id' => (int)$id ]);
	}

	/**
	 * @param int $id
	 */
	public static function delete_question($id) {
		global $wpdb;
		$wpdb->delete( self::question_table_name(), ['id' => (int)$id ]);
	}

	/**
	 * @param int $id
	 */
	public static function delete_answer($id) {
		global $wpdb;
		$wpdb->delete( self::question_answer_table_name(), ['id' => (int)$id ]);
	}

	/**
	 * @return array
	 */
	public static function list_tests() {
		global $wpdb;
		return $wpdb->get_results( 'SELECT * FROM ' . self::survey_table_name() . ' q ORDER BY q.title', ARRAY_A );
	}

	/**
	 * @param int $id
	 *
	 * @return array
	 */
	public static function list_questions($id) {
		global $wpdb;
		return $wpdb->get_results( 'SELECT * FROM ' . self::question_table_name() . ' q WHERE q.survey_id=' . (int)$id . ' ORDER BY q.content ASC', ARRAY_A );
	}

	/**
	 * @param int $id
	 *
	 * @return array
	 */
	public static function list_answers($id) {
		global $wpdb;
		return $wpdb->get_results( 'SELECT * FROM ' . self::question_answer_table_name() . ' qa WHERE qa.question_id=' . (int)$id, ARRAY_A );
	}

	/**
	 * @param int $id
	 *
	 * @return bool
	 */
	public static function has_test($id) {
		global $wpdb;
		return (bool)$wpdb->get_var('SELECT COUNT(*) FROM ' . self::survey_table_name() . ' t WHERE t.id = ' . (int) $id );
	}

	/**
	 * @param int $id
	 *
	 * @return bool
	 */
	public static function has_question($id) {
		global $wpdb;
		return (bool)$wpdb->get_var('SELECT COUNT(*) FROM ' . self::question_table_name() . ' t WHERE q.id = ' . (int) $id );
	}

	/**
	 * @param int $id
	 * @param int $limit
	 *
	 * @return array
	 */
	public static function random_question( $id, $limit ) {
		global $wpdb;
		$sql = 'SELECT * FROM ' . self::question_table_name() . ' q WHERE is_enabled=1 AND q.survey_id=' . (int)$id . ' ORDER BY RAND() LIMIT ' . (int)$limit;
		return $wpdb->get_results( $sql, ARRAY_A );
	}

	/**
	 * @param int $id
	 *
	 * @return array
	 */
	public static function random_answers( $id ) {
		global $wpdb;
		$sql = 'SELECT * FROM ' . self::question_answer_table_name() . ' qa WHERE qa.question_id=' . (int)$id . ' ORDER BY RAND()';
		return $wpdb->get_results( $sql, ARRAY_A );
	}

	/**
	 * @param $id
	 *
	 * @return array
	 */
	public static function get_valid_answers( $id ) {
		global $wpdb;
		$sql = 'SELECT * FROM ' . self::question_answer_table_name() . ' qa WHERE qa.question_id=' . (int)$id . ' AND is_valid=1';
		return $wpdb->get_results( $sql, ARRAY_A );
	}

	/**
	 * @param int $test_id
	 * @param int $user_id
	 *
	 * @return array
	 */
	public static function get_unfinished_test($test_id, $user_id) {
		global $wpdb;
		$sql = 'SELECT * FROM ' . self::result_table_name() . ' r WHERE finished_at IS NULL AND r.survey_id = ' . (int) $test_id . ' AND r.user_id = ' . (int) $user_id;
		return $wpdb->get_row($sql, ARRAY_A);
	}

	/**
	 * @param int $test_id
	 * @param int $user_id
	 * @param int $limit
	 *
	 * @return array
	 */
	public static function start_new_test($test_id, $user_id, $limit) {
		global $wpdb;
		$data = [];

		foreach (self::random_question( $test_id, $limit) as $question) {
			$data['questions'][$question['id']] = [];
		}

		$data = json_encode( $data, JSON_UNESCAPED_UNICODE );

		$wpdb->insert( self::result_table_name(), [
			'user_id' => $user_id,
			'survey_id' => $test_id,
			'data' => $data,
			'started_at' => date('Y-m-d H:i:s')
		] );

		return self::get_unfinished_test( $test_id, $user_id);
	}

	public static function finish_result($id, $questions, $value) {
		global $wpdb;
		$result = self::get_result( $id);
		$data = json_decode( $result['data'], true );

		foreach ($questions as $question_id => $answers) {
			$data['questions'][$question_id] = $answers;
		}

		$data = json_encode( $data, JSON_UNESCAPED_UNICODE );
		$wpdb->update( self::result_table_name(), [
			'data' => $data,
			'value' => $value,
			'finished_at' => date('Y-m-d H:i:s')
		], ['id' => $id]);

		return self::get_result( $id);

	}
}
