<?php

/**
 * @since      1.0.0
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
class Wp_Survey_Helper {
	/**
	 * @param string $template
	 * @param array $data
	 * @param bool $return
	 *
	 * @return string|null
	 */
	public static function admin_render($template, $data = [], $return = false) {
		$path = plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials';
		$template = trim( $template , '/' );

		$html = self::render_internal( $path . '/' . $template, $data);

		if ( $return ) {
			return $html;
		}

		echo $html;

		return null;
	}
	/**
	 * @param string $template
	 * @param array $data
	 * @param bool $return
	 *
	 * @return string|null
	 */
	public static function public_render($template, $data = [], $return = false) {
		$path = plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials';
		$template = trim( $template , '/' );

		$html = self::render_internal( $path . '/' . $template, $data);

		if ( $return ) {
			return $html;
		}

		echo $html;

		return null;
	}


	public static function call_404($msg) {
		_default_wp_die_handler($msg, '', ['response' => 404]);
	}


	/**
	 * @param array $params
	 *
	 * @return string
	 */
	public static function admin_url($params = []) {
		$url = admin_url('admin.php?page=wp-survey');
		$url .= self::array2url( $params );
		return $url;
	}

	/**
	 * @param array $array
	 *
	 * @return string
	 */
	private static function array2url($array) {
		$url = '';
		$exclude_value = ['', false, null];

		foreach ($array as $key => $value) {
			if ( empty( $key ) ) {
				$key = $value;
				$value = null;
			}

			$url .= '&' . urlencode( $key );

			if ( !in_array( $value, $exclude_value, true ) ) {
				$url .= '=' . urlencode( $value );
			}
		}

		return $url;
	}


	/**
	 * @param string $file
	 * @param array $data
	 *
	 * @return false|string
	 */
	private static function render_internal($file, $data) {
		if ( is_array( $data ) ){
			extract( $data );
		}

		ob_start();
		include $file;
		return ob_get_clean();
	}
}
