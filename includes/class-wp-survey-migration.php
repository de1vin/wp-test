<?php

/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 */

/**
 * Applying and rolling back migrations.
 *
 * @since      1.0.0
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
class Wp_Survey_Migration {
	/**
	 * @since    1.0.0
	 */
	public static function up() {
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

		if($wpdb->get_var('SHOW TABLES LIKE "' . Wp_Survey_DB::survey_table_name() . '"') != Wp_Survey_DB::survey_table_name()) {
			$sql = 'CREATE TABLE `' . Wp_Survey_DB::survey_table_name() . '`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` text NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ' . $charset_collate;
			dbDelta( $sql );
		}

		if($wpdb->get_var('SHOW TABLES LIKE "' . Wp_Survey_DB::question_table_name() . '"') != Wp_Survey_DB::question_table_name()) {
			$sql = 'CREATE TABLE `' . Wp_Survey_DB::question_table_name() . '`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content` text NULL,
  `is_enabled` tinyint(1) UNSIGNED NULL DEFAULT 1,
  `survey_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `wpt_survey_id`(`survey_id`) USING BTREE,
  CONSTRAINT `wpt_survey_id_uk` FOREIGN KEY (`survey_id`) REFERENCES `wp_survey` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ' . $charset_collate;
			dbDelta( $sql );
		}

		if($wpdb->get_var('SHOW TABLES LIKE "' . Wp_Survey_DB::question_answer_table_name() . '"') != Wp_Survey_DB::question_answer_table_name()) {
			$sql = 'CREATE TABLE `' . Wp_Survey_DB::question_answer_table_name() . '`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `content` text NULL,
  `is_valid` tinyint(1) UNSIGNED NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `wpt_qa_question_id`(`question_id`) USING BTREE,
  CONSTRAINT `wpt_qa_question_id_uk` FOREIGN KEY (`question_id`) REFERENCES `' . Wp_Survey_DB::question_table_name() . '` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ' . $charset_collate;
			dbDelta( $sql );
		}

		if($wpdb->get_var('SHOW TABLES LIKE "' . Wp_Survey_DB::result_table_name() . '"') != Wp_Survey_DB::result_table_name()) {
			$sql = 'CREATE TABLE `' . Wp_Survey_DB::result_table_name() . '`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `survey_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `started_at` datetime NOT NULL,
  `finished_at` datetime NULL DEFAULT NULL,
  `value` float NULL DEFAULT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 ' . $charset_collate;
			dbDelta( $sql );
		}
	}

	/**
	 * @since    1.0.0
	 */
	public static function down() {
		$sql[] = 'DROP TABLE IF EXISTS `' . Wp_Survey_DB::question_answer_table_name() . '`';
		$sql[] = 'DROP TABLE IF EXISTS `' . Wp_Survey_DB::question_table_name() . '`';
		$sql[] = 'DROP TABLE IF EXISTS `' . Wp_Survey_DB::survey_table_name() . '`';
		$sql[] = 'DROP TABLE IF EXISTS `' . Wp_Survey_DB::result_table_name() . '`';

		global $wpdb;
		foreach ($sql as $code) {
			$wpdb->query($code);
		}
	}
}
