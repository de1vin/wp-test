<?php

/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
class Wp_Survey_Activator {

	/**
	 * @since    1.0.0
	 */
	public static function activate() {
		self::add_options();
		Wp_Survey_Migration::up();
	}


	public static function default_options() {
		$options_prefix = 'wpt_';

		return [
			$options_prefix . 'version' => WP_TESTING_VERSION,
			$options_prefix . 'questions_per_test' => 10,
		];
	}

	private static function add_options() {
		foreach (self::default_options() as $option_name => $option) {
			if ( get_option( $option_name ) === false ) {
				add_option($option_name, $option);
			}
		}
	}
}
