<?php

/**
 * Fired during plugin deactivation
 *
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Survey
 * @subpackage Wp_Survey/includes
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
require_once plugin_dir_path( __FILE__ ) . 'class-wp-survey-activator.php';
class Wp_Survey_Deactivator {

	/**
	 * @since    1.0.0
	 */
	public static function deactivate() {
	}


	public static function uninstall() {
		$options_name = array_keys( Wp_Survey_Activator::default_options() );

		foreach ( $options_name as $option_name ) {
			delete_option( $option_name );
		}
		Wp_Survey_Migration::down();
	}
}
