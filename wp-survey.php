<?php

/**
 * The plugin bootstrap file
 *
 * @since             1.0.0
 * @package           Wp_Survey
 *
 * @wordpress-plugin
 * Plugin Name:       WP-Survey
 * Description:       Survey Plugin. Use the shortcode to display the test:   [wp_survey id="TEST_ID"] or [wp_survey id="TEST_ID" limit="QUESTIONS_LIMIT"]
 * Version:           1.0.0
 * Author:            Zinchenko Vladimir
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-survey
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 */
define( 'WP_TESTING_VERSION', '1.0.0' );


function activate_wp_survey() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-survey-activator.php';
	Wp_Survey_Activator::activate();
}


function deactivate_wp_survey() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-survey-deactivator.php';
	Wp_Survey_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_survey' );
register_deactivation_hook( __FILE__, 'deactivate_wp_survey' );
register_uninstall_hook( __FILE__, ['Wp_Survey_Deactivator', 'uninstall'] );


require plugin_dir_path( __FILE__ ) . 'includes/class-wp-survey.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_survey() {

	$plugin = Wp_Survey::getInstance();
	$plugin->run();
}
run_wp_survey();
