<?php

/**
 * The admin-specific actions of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/admin
 */

/**
 * The admin-specific actions of the plugin.
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/admin
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
class Wp_Survey_Admin_Controller {
	public function action_index() {
		$tests = Wp_Survey_DB::list_tests();
		Wp_Survey_Helper::admin_render( 'index.php', [
			'tests' => $tests
		] );
	}

	public function action_test() {
		$test = $this->find_test();
		$questions = Wp_Survey_DB::list_questions($_GET['id']);

		Wp_Survey_Helper::admin_render( 'test.php', [
			'test' => $test,
			'questions' => $questions
		] );
	}

	public function action_add_test() {
		$test = [
			'title' => ''
		];
		$errors = [];

		if (isset( $_POST['test'] )) {
			$test = array_merge( $test, $_POST['test'] );
			$title = trim($test['title']);
			$test_id = Wp_Survey_DB::add_test( $title );

			wp_redirect(Wp_Survey_Helper::admin_url([ 'action' =>  'test', 'id' => $test_id ]));
			exit;
		}

		Wp_Survey_Helper::admin_render( 'add_test.php' , [
			'test' => $test,
			'errors' => $errors,
		]);
	}

	public function action_add_question() {
		$question = [
			'content' => '',
			'answer' => []
		];
		$errors = [];

		$test = $this->find_test();

		if (isset( $_POST['question'])) {
			$question = array_merge( $question, $_POST['question'] );
			$content = trim($question['content']);
			$question_id = Wp_Survey_DB::add_question( $test['id'], $content );

			foreach ($question['answer'] as $answer) {
				if (!isset($answer['content'])) {
					continue;
				}
				$content = trim($answer['content']);
				if ($content) {
					$is_valid = isset( $answer['is_valid'] );
					Wp_Survey_DB::add_answer( $question_id, $content, $is_valid);
				}
			}

			wp_redirect(Wp_Survey_Helper::admin_url([ 'action' =>  'test', 'id' => $test['id'] ]));
			exit;
		}

		Wp_Survey_Helper::admin_render( 'add_question.php' , [
			'test' => $test,
			'question' => $question,
			'errors' => $errors,
		]);
	}

	public function action_edit_test() {
		$test = $this->find_test();
		$errors = [];

		if (isset( $_POST['test'] )) {
			$test = array_merge( $test, $_POST['test'] );
			$title = trim($test['title']);
			Wp_Survey_DB::update_test( $test['id'], $title );

			wp_redirect(Wp_Survey_Helper::admin_url([ 'action' =>  'test', 'id' => $test['id'] ]));
			exit;
		}

		Wp_Survey_Helper::admin_render( 'edit_test.php' , [
			'test' => $test,
			'errors' => $errors,
		]);
	}

	public function action_edit_question() {
		$question = $this->find_question();
		$question['answer'] = Wp_Survey_DB::list_answers( $question['id'] );
		$test = $this->find_test($question['survey_id']);
		$errors = [];

		if (isset( $_POST['question'])) {
			$question = array_merge($question, $_POST['question']);
			$content = trim($question['content']);
			Wp_Survey_DB::update_question( $question['id'], $content );
			$question_id = $question['id'];

			foreach ($question['answer'] as $answer) {
				if (isset( $answer['remove']) && $answer['remove']) {
					Wp_Survey_DB::delete_answer( $answer['id'] );
					continue;
				}
				if (!isset($answer['content'])) {
					continue;
				}

				$content = trim($answer['content']);
				$is_valid = isset( $answer['is_valid'] );

				if (isset( $answer['id'] )) {
					Wp_Survey_DB::update_answer( $answer['id'], $content, $is_valid);
					var_dump( 'update' .$answer['id'] );
				} else {
					Wp_Survey_DB::add_answer( $question_id, $content, $is_valid);
					var_dump( 'new');
				}
			}

			wp_redirect(Wp_Survey_Helper::admin_url([ 'action' =>  'test', 'id' => $test['id'] ]));
			exit;
		}

		Wp_Survey_Helper::admin_render( 'edit_question.php' , [
			'test' => $test,
			'question' => $question,
			'errors' => $errors,
		]);
	}

	public function action_delete_test() {
		if (isset( $_GET['id'] )) {
			Wp_Survey_DB::delete_test( $_GET['id'] );
		}

		wp_redirect(Wp_Survey_Helper::admin_url());
	}

	public function action_delete_question() {
		$question = $this->find_question();
		Wp_Survey_DB::delete_question( $_GET['id'] );

		wp_redirect(Wp_Survey_Helper::admin_url( [ 'action' => 'test', 'id' => $question['survey_id'] ] ));
	}

	public function action_results() {
		Wp_Survey_Helper::admin_render( 'results.php' );
	}

	/**
	 * @param null $id
	 *
	 * @return array
	 */
	private function find_test($id = null) {
		if ($id === null && isset( $_GET['id'] )) {
			$id = $_GET['id'];
		}
		if ($id) {
			$record = Wp_Survey_DB::get_test( $id );
			if ($record) {
				return $record;
			}
		}

		Wp_Survey_Helper::call_404( 'Test not found.' );
		exit();
	}

	/**
	 * @param null $id
	 *
	 * @return array
	 */
	private function find_question($id = null) {
		if ($id === null && isset( $_GET['id'] )) {
			$id = $_GET['id'];
		}
		if ($id) {
			$record = Wp_Survey_DB::get_question( $id );
			if ($record) {
				return $record;
			}
		}

		Wp_Survey_Helper::call_404( 'Question not found.' );
		exit();
	}
}
