<?php

/**
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/admin/partials
 */
?>
<div class="wrap">
    <h1 class="wp-heading-inline">WP Survey</h1>
    <h2>
        <?php echo htmlentities( $test['title'] )?>
        <a href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'add_question', 'id' => $test['id']])?>" class="page-title-action">Add question</a>
        <a href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'edit_test', 'id' => $test['id'] ])?>" class="page-title-action">Edit</a>
    </h2>
    <hr class="wp-header-end">
    <table class="wp-list-table widefat fixed striped wpt-table">
        <?php foreach ($questions as $question): ?>
        <tr class="entry">
            <td class="col-id">
		        <?php echo (int)$question['id']?>
            </td>
            <td class="title column-title has-row-actions column-primary">
                <a href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'edit_question', 'id' => (int)$question['id']]) ?>"><?php echo htmlentities( $question['content'] )?></a>
            </td>
            <td class="col-actions">
                <a title="edit" href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'edit_question', 'id' => (int)$question['id']])?>" class="wpt-edit-btn"><span class="dashicons dashicons-edit"></span></a>
                <a title="remove" href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'delete_question', 'id' => (int)$question['id']])?>" class="wpt-remove-btn"><span class="dashicons dashicons-no-alt"></span></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>

