<?php

/**
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/admin/partials
 */
?>
<div class="wrap">
    <h1 class="wp-heading-inline">Add new question <small><a href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'test', 'id' => (int)$test['id']]) ?>"><?php echo htmlentities( $test['title'] )?></a></small></h1>
    <hr class="wp-header-end">
    <?php Wp_Survey_Helper::admin_render( '_question_form.php', [
	    'question' => $question,
	    'errors' => $errors,
    ])?>
</div>
