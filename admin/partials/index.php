<?php

/**
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/admin/partials
 */
?>
<div class="wrap">
    <h1 class="wp-heading-inline">WP Survey</h1>
    <a href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'add_test'])?>" class="page-title-action">Add test</a>
    <hr class="wp-header-end">
    <table class="wp-list-table widefat fixed striped wpt-table">
        <?php foreach ($tests as $test): ?>
        <tr class="entry">
            <td class="col-id">
                <?php echo (int)$test['id']?>
            </td>
            <td class="title column-title column-primary">
                <a href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'test', 'id' => (int)$test['id']]) ?>"><?php echo htmlentities( $test['title'] )?></a>
                <div class="wpt-shortcode"><code>[wp_survey id="<?php echo (int)$test['id']?>"]</code></div>
            </td>
            <td class="col-actions">
                <a title="edit" href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'edit_test', 'id' => (int)$test['id']])?>" class="wpt-edit-btn"><span class="dashicons dashicons-edit"></span></a>
                <a title="remove" href="<?php echo Wp_Survey_Helper::admin_url(['action' => 'delete_test', 'id' => (int)$test['id']])?>" class="wpt-remove-btn"><span class="dashicons dashicons-no-alt"></span></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>

