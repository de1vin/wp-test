<?php

/**
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/admin/partials
 */
?>
<div class="wrap">
    <h1 class="wp-heading-inline">Edit test</h1>
    <hr class="wp-header-end">
    <?php Wp_Survey_Helper::admin_render( '_test_form.php', [
	    'test' => $test,
	    'errors' => $errors,
    ])?>
</div>
