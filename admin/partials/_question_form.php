<form action="" method="post" class="wpt-admin-form">
	<h2 class="title">Question text</h2>
	<textarea required="required" name="question[content]" class="large-text code" rows="5"><?php echo $question['content'] ?></textarea>
	<h2 class="title">Answers <a href="" class="page-title-action wpt-admin wpt-admin-form__add-answer">Add answer</a></h2>

	<div class="wpt-admin-form__answers" data-answer-index="<?php echo count($question['answer']); ?>">
		<?php foreach ($question['answer'] as $n => $answer) : ?>
		<div class="wpt-admin-form__row">
			<input type="hidden" name="question[answer][<?php echo $n ?>][id]" value="<?php echo $answer['id'] ?>">
			<input type="hidden" class="wpt-admin-form__answer-remove" name="question[answer][<?php echo $n ?>][remove]" value="0">
			<div class="wpt-admin-form__valid-col">
				<input class="wpt-answer-is-valid" type="checkbox" name="question[answer][<?php echo $n ?>][is_valid]" value="1" <?php if ($answer['is_valid']) echo 'checked' ?>>
			</div>
			<div class="wpt-admin-form__answer-col">
				<input type="text" name="question[answer][<?php echo $n ?>][content]" class="large-text code" value="<?php echo $answer['content'] ?>" autocomplete="off" required="required">
			</div>
			<div class="wpt-admin-form__valid-col">
				<a href="#" class="wpt-admin-form__answer-remove-btn"><span class="dashicons dashicons-no-alt"></span></a>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
	<p class="submit">
		<input type="submit" name="submit" id="submit" class="button button-primary" value="Save">
	</p>
</form>

<div class="wpt-admin-form__answer-template" style="display: none">
	<div class="wpt-admin-form__row">
		<div class="wpt-admin-form__valid-col">
			<input class="wpt-answer-is-valid" type="checkbox" name="question[answer][__index__][is_valid]" value="1">
		</div>
		<div class="wpt-admin-form__answer-col">
			<input type="text" name="question[answer][__index__][content]" class="large-text code" value="" autocomplete="off" required="required">
		</div>
		<div class="wpt-admin-form__valid-col">
			<a href="#" class="wpt-admin-form__answer-remove-btn"><span class="dashicons dashicons-no-alt"></span></a>
		</div>
	</div>
</div>
