<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Survey
 * @subpackage Wp_Survey/admin
 * @author     Zinchenko Vladimir <delvin@tagan.ru>
 */
class Wp_Survey_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-survey-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-survey-admin.js', array( 'jquery' ), $this->version, false );
	}


	public function admin_init() {
		global $pagenow;
		if (( $pagenow == 'admin.php' ) && ($_GET['page'] == 'wp-survey')) {
			ob_start();
//			$this->router();
		}
	}


	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function add_menu() {
		add_menu_page(
			'WP Survey',
			'WP Survey',
			'edit_others_pages',
			'wp-survey',
			[$this, 'router'],
			'dashicons-chart-bar',
			20
		);

		global $submenu;
		$submenu['wp-survey'][] = [ 'Test management', 'manage_options', Wp_Survey_Helper::admin_url() ];
		$submenu['wp-survey'][] = [ 'Add test', 'manage_options', Wp_Survey_Helper::admin_url(['action' => 'add_test']) ];
		$submenu['wp-survey'][] = [ 'View results', 'manage_options', Wp_Survey_Helper::admin_url(['action' => 'results']) ];
	}

	public function router() {
		$action = isset( $_GET['action'] ) ? $_GET['action'] : 'index';
		$action = 'action_' . $action;

		$controller = new Wp_Survey_Admin_Controller();

		if (method_exists( $controller, $action )) {
			$controller->{$action}();
		} else {
			Wp_Survey_Helper::call_404('Action not found');
		}

		global $pagenow;
		if (( $pagenow == 'admin.php' ) && ($_GET['page'] == 'wp-survey')) {
			echo ob_get_clean();
		}
	}


	public function add_options() {
		register_setting( 'general', 'wpt_questions_per_test', ['type' => 'integer', 'default' => 10 ]);
		add_settings_field(
			'wpt_questions_per_test',
			'Default questions per test',
			['Wp_Survey_Options', 'render_settings_fields'],
			'general'
		);
	}
}
