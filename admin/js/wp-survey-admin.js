(function( $ ) {
	'use strict';
	/*$(document).on("submit", ".wpt-admin-form", function (e) {
		e.preventDefault();
		const form = $(".wpt-admin-form");
		$.ajax({
			url: form.attr("action"),
			method: "post"
		}).done(function(data) {
			console.log(data);
		});
	})*/
	$(document).on("click", ".wpt-remove-btn", function (e) {
		e.preventDefault();
		$.ajax({
			url: $(this).attr("href"),
			method: "post"
		}).done(function () {
			window.location.reload(true);
		});
	});
	$(document).on("click", ".wpt-admin-form__answer-remove-btn", function (e) {
		e.preventDefault();
		const row = $(this).parents(".wpt-admin-form__row");
		const remover = row.find(".wpt-admin-form__answer-remove");
		if (remover) {
			remover.val(1);
			row.hide();
		} else {
			row.remove();
		}
	});
	$(document).on("click", ".wpt-admin-form__add-answer", function (e) {
		e.preventDefault();
		const container = $(".wpt-admin-form__answers");
		let tpl = $(".wpt-admin-form__answer-template").html();
		let index = container.data("answer-index");
		tpl = tpl.replace(/__index__/g, index);
		tpl = $(tpl);
		// if (container.find(".wpt-answer-is-valid:checked").length == 0) {
		// 	tpl.find(".wpt-answer-is-valid").prop('checked', true);
		// }

		container.append(tpl);
		index++;
		container.data("answer-index", index);
	});
})( jQuery );
